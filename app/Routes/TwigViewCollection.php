<?php
namespace App\Routes;

use SlaxWeb\Router\Route;
use SlaxWeb\Bootstrap\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TwigViewCollection extends \SlaxWeb\Bootstrap\Service\RouteCollection
{
    public function define()
    {
        $this->routes[] = [
            "uri"       =>  "twig/loadView/[:named:]",
            "method"    =>  Route::METHOD_GET,
            "action"    =>  function (
                Request $request,
                Response $response,
                Application $app
            ) {
                $app["loadController.service"]("Twig")->loadView();
            }
        ];

        $this->routes[] = [
            "uri"       =>  "twig/loadTemplate/[:named:]",
            "method"    =>  Route::METHOD_GET,
            "action"    =>  function (
                Request $request,
                Response $response,
                Application $app
            ) {
                $app["loadController.service"]("Twig")->loadTemplate();
            }
        ];

        $this->routes[] = [
            "uri"       =>  "twig/withLayout/",
            "method"    =>  Route::METHOD_GET,
            "action"    =>  function (
                Request $request,
                Response $response,
                Application $app
            ) {
                $app["loadController.service"]("Twig")->withLayout();
            }
        ];
    }
}
