<?php
namespace App\Routes;

use SlaxWeb\Router\Route;
use SlaxWeb\Bootstrap\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class SessionCollection extends \SlaxWeb\Bootstrap\Service\RouteCollection
{
    public function define()
    {
        $this->routes[] = [
            "uri"       =>  "session/write/[:named:]",
            "method"    =>  Route::METHOD_GET,
            "action"    =>  function (
                Request $request,
                Response $response,
                Application $app
            ) {
                $app["loadController.service"]("Session")->write();
            }
        ];

        $this->routes[] = [
            "uri"       =>  "session/read/[:named:]",
            "method"    =>  Route::METHOD_GET,
            "action"    =>  function (
                Request $request,
                Response $response,
                Application $app
            ) {
                $app["loadController.service"]("Session")->read();
            }
        ];
    }
}
