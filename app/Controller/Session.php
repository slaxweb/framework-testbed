<?php
namespace App\Controller;

class Session extends \SlaxWeb\Bootstrap\Controller\Base
{
    protected $session = null;
    protected $request = null;
    protected $output = null;

    public function init()
    {
        $this->app["outputHandler"] = "json";
        $this->request = $this->app["request.service"];
        $this->session = $this->app["session.service"];
        $this->output = $this->app["output.service"];
    }

    public function write()
    {
        $this->session->set(
            "sessdata",
            ($data = $this->request->get("sessdata", null))
        );
        $this->output->add(
            ["message" => "Session data written", "data" => $data]
        );
    }

    public function read()
    {
        if (($sessParam = $this->request->get("sessparam", "")) === "") {
            $this->output->addError("Expected 'sessparam' parameter not provided");
            return;
        }

        $sessExpect = $this->request->get("expected", "");
        $sessData = $this->session->get($sessParam, null);

        if ($sessData === null) {
            $this->output->addError("Session holds no data for '{$sessParam}' variable.");
            return;
        }

        if ($sessExpect === "") {
            $this->output->addError(
                "Expected 'expected' parameter not provided, comparison not possible"
            );
        } elseif ($sessData !== $sessExpect) {
            $this->output->addError("Session variable '{$sessParam}' does not hold expected data");
        }

        $this->output->add([
            "expected"  =>  $sessExpect,
            "sessparam" =>  $sessParam,
            "value"     =>  $sessData
        ]);
    }
}
