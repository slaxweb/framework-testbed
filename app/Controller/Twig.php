<?php
namespace App\Controller;

class Twig extends \SlaxWeb\Bootstrap\Controller\Base
{
    protected $request = null;
    protected $output = null;

    public function init()
    {
        $this->request = $this->app["request.service"];
        $this->output = $this->app["output.service"];
    }

    public function loadView()
    {
        $view = $this->app["loadView.service"]("TwigTest");
        $this->output
            ->add($view)
            ->addData(["testdata" => $this->request->get("testData", "")]);
    }

    public function loadTemplate()
    {
        $template = $this->app["loadTemplate.service"]("TwigTest");
        $this->output
            ->add($template)
            ->addData(["testdata" => $this->request->get("testData", "")]);
    }

    public function withLayout()
    {
        $view = $this->app["loadView.service"]("TwigTest");
        $layout = $this->app["loadTemplate.service"]("TwigLayout");
        $view->setLayout($layout);
        $this->output
            ->add($view)
            ->addData(["testdata" => "in layout"]);
    }
}
